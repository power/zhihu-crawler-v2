package org.har01d.crawler;

@FunctionalInterface
public interface Crawler {
    void crawler() throws InterruptedException;
}
